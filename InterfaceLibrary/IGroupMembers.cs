﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace InterfaceLibrary
{
    public interface IGroupMembers
    {
        public int SportsmanId { get; set; }      // внешний ключ
        public int GroupId { get; set; }      // внешний ключ
    }
}
