﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLibrary
{
    public interface ICoach
    {
        public string FIO { get; set; }
        public string INN { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
