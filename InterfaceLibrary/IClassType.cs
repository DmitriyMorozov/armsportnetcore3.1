﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLibrary
{
    public interface IClassType
    {
        public string Name { get; set; }
    }
}
