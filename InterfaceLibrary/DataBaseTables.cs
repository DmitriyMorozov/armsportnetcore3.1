﻿using InterfaceLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseApi
{
    public class User : BaseModel, IUser
    {
        [Required]
        public string Login { get; set; }
        private string _password;

        [Required]
        public string Password 
        { 
            get 
            {
                return _password;
            }
            set
            {
                SHA256 sha256 = SHA256.Create();
                byte[] hashValue = sha256.ComputeHash(Encoding.UTF8.GetBytes(value));
                _password = BitConverter.ToString(hashValue).Replace("-", "");
            }
        }
    }

    public class Sportsman : BaseModel, ISportsman
    {
        [Required]
        [RegularExpression("^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$|^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$")]
        [MaxLength(50)]
        public string FIO { get; set; }

        [Required]
        [MaxLength(11)]
        [MinLength(11)]
        [RegularExpression("^[0-9]*$")]
        public string INN { get; set; }

        [Required]
        [MaxLength(12)]
        [MinLength(12)]
        [RegularExpression("^\\+7\\d{10}$")]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
    }

    public class Coach : BaseModel, ICoach
    {
        [Required]
        [RegularExpression("^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$|^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$")]
        [MaxLength(50)]
        public string FIO { get; set; }

        [Required]
        [MaxLength(11)]
        [MinLength(11)]
        [RegularExpression("^[0-9]*$")]
        public string INN { get; set; }

        [Required]
        [MaxLength(12)]
        [MinLength(12)]
        [RegularExpression("^\\+7\\d{10}$")]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
    }

    public class ClassType : BaseModel, IClassType
    {
        [Required]
        [MaxLength(25)]
        public string Name { get; set; }
    }

    public class Group : BaseModel, IGroup
    {
        [Required]
        [MaxLength(15)]
        public string Name { get; set; }

        [Required]
        public int ClassTypeId { get; set; }      // внешний ключ
    }

    public class GroupMembers : BaseModel, IGroupMembers
    {
        [Required]
        public int SportsmanId { get; set; }      // внешний ключ

        [Required]
        public int GroupId { get; set; }      // внешний ключ
    }

    public class RoomGym : BaseModel, IRoomGym
    {
        [Required]
        [MaxLength(25)]
        public string Name { get; set; }
    }

    public class TimeTable : BaseModel, ITimeTable
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        public int RoomGymId { get; set; }      // внешний ключ

        [Required]
        [DataType(DataType.Time)]
        public DateTime TimeStart { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public DateTime TimeEnd { get; set; }

        [Required]
        public int CoachId { get; set; }      // внешний ключ

        [Required]
        public int GroupId { get; set; }      // внешний ключ
    }
}