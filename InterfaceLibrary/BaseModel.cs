﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace InterfaceLibrary
{
    public class BaseModel
    {
        [Key]
        public int Id { get; set; }
    }
}
