﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Text;
using DataBaseApi;

namespace InterfaceLibrary
{
    public interface IRestClient
    {
        public string GetContent();
        public bool IsOk();
        public void SetToken(string token);
        public bool Post<T>(string apiPath, T tableObject) where T : class;
        public List<T> Get<T>(string apiPath) where T : class;
        public T Get<T>(string apiPath, int id) where T : class;
        public bool Put<T>(string apiPath, T tableObject) where T : BaseModel;
        public bool Delete(string apiPath, int id);
    }
}
