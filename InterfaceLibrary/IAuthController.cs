﻿using DataBaseApi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace InterfaceLibrary
{
    public interface IAuthController
    {
        Task<IActionResult> Register(User user);
        Task<IActionResult> Login(User user);
    }
}
