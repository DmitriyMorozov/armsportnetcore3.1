﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLibrary
{
    public interface IUser
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
