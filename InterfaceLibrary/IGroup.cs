﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLibrary
{
    public interface IGroup
    {
        public string Name { get; set; }

        public int ClassTypeId { get; set; }
    }
}
