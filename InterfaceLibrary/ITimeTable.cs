﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLibrary
{
    public interface ITimeTable
    {
        public DateTime Date { get; set; }
        public int RoomGymId { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public int CoachId { get; set; }
        public int GroupId { get; set; }
    }
}
