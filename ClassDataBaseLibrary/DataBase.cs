﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Text.RegularExpressions;
using System.Xml;

namespace DataBaseApi
{
    public class DataBase : DbContext
    {
        private string dataBaseName = string.Empty;

        private DbSet<Sportsman> Sportsman { get; set; } = null!;
        private DbSet<Coach> Coach { get; set; } = null!;
        private DbSet<ClassType> ClassType { get; set; } = null!;
        private DbSet<Group> Group { get; set; } = null!;
        private DbSet<GroupMembers> GroupMembers { get; set; } = null!;
        private DbSet<RoomGym> RoomGym { get; set; } = null!;
        private DbSet<TimeTable> TimeTable { get; set; } = null!;
        private DbSet<User> UserTable { get; set; } = null!;

        public DataBase(string dataBaseName)
        {
            this.dataBaseName = dataBaseName;

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={dataBaseName}.db");
        }

    }
}
