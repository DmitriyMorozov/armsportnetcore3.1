﻿using DataBaseApi;
using System;

namespace DatabaseCreationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string databaseName = "TEST";

            using (var dbContext = new DataBase(databaseName))
            {
               
                dbContext.Database.EnsureCreated();

               
                dbContext.SaveChanges();

                Console.WriteLine("Бд создана.");
            }

            
            Console.ReadKey();
        }
    }
}
