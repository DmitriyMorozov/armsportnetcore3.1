using DataBaseApi;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Microsoft.Win32;
using RestAPINext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationNext
{

    //public class MariaDBContext : DbContext
    //{
    //    public MariaDBContext(DbContextOptions<MariaDBContext> options) : base(options) { Database.EnsureCreated(); }

    //    private DbSet<Sportsman> Sportsman { get; set; } = null!;
    //    private DbSet<Coach> Coach { get; set; } = null!;
    //    private DbSet<ClassType> ClassType { get; set; } = null!;
    //    private DbSet<Group> Group { get; set; } = null!;
    //    private DbSet<GroupMembers> GroupMembers { get; set; } = null!;
    //    private DbSet<RoomGym> RoomGym { get; set; } = null!;
    //    private DbSet<TimeTable> TimeTable { get; set; } = null!;
    //    private DbSet<User> UserTable { get; set; } = null!;

    //    protected override void OnModelCreating(ModelBuilder modelBuilder)
    //    {
    //        modelBuilder.Entity<Sportsman>().ToTable("Sportsman");
    //        modelBuilder.Entity<Coach>().ToTable("Coach");
    //        modelBuilder.Entity<ClassType>().ToTable("ClassType");
    //        modelBuilder.Entity<Group>().ToTable("Group");
    //        modelBuilder.Entity<GroupMembers>().ToTable("GroupMembers");
    //        modelBuilder.Entity<RoomGym>().ToTable("RoomGym");
    //        modelBuilder.Entity<TimeTable>().ToTable("TimeTable");
    //        modelBuilder.Entity<User>().ToTable("User");
    //    }
    //}

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //var test = Configuration.GetSection("Settings").Get<DataBaseSetting>().DataBaseName;

            /*
            var AppName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("DataBaseSetting");

            int i = 1;8*/

            string regKey = "Software\\ABOBUS\\ABOBUSAPI";

            using (var key = Registry.CurrentUser.OpenSubKey(regKey, false)) // False is important!
            {
                var s = key?.GetValue("DataBaseType") as string;
                //if (!string.IsNullOrWhiteSpace(s))
                //{
                //    var version = new Version(s);
                //}

                int i = 1;
            }


            //string connectionString = "server=localhost;user=root;password=12;database=marinka";
            //MySqlServerVersion serverVersion = new MySqlServerVersion(new Version(10, 9, 8));

            //var dbContextOptions = new DbContextOptionsBuilder<MariaDBContext>();
            //dbContextOptions.UseMySql(connectionString, serverVersion);

            //var testMaria = new MariaDBContext(dbContextOptions.Options);

            //services.AddDbContext<MariaDBContext>(
            //    dbContextOptions => dbContextOptions
            //        .UseMySql(connectionString, serverVersion)
            //        // The following three options help with debugging, but should
            //        // be changed or removed for production.
            //        .LogTo(Console.WriteLine, LogLevel.Information)
            //        .EnableSensitiveDataLogging()
            //        .EnableDetailedErrors()
            //);

            services.AddSwaggerGen();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Swagger UI",
                    Description = "API",
                });
            });

            services.AddAuthorization();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            // ���������, ����� �� �������������� �������� ��� ��������� ������
            ValidateIssuer = true,
            // ������, �������������� ��������
            ValidIssuer = AuthOptions.ISSUER,
            // ����� �� �������������� ����������� ������
            ValidateAudience = true,
            // ��������� ����������� ������
            ValidAudience = AuthOptions.AUDIENCE,
            // ����� �� �������������� ����� �������������
            ValidateLifetime = true,
            // ��������� ����� ������������
            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
            // ��������� ����� ������������
            ValidateIssuerSigningKey = true,
        };
    });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Showing API V1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();   // ���������� middleware �������������� 
            app.UseAuthorization();   // ���������� middleware ����������� 

            //app.Map("/", ret)

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
