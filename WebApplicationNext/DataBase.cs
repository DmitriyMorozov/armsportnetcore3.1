﻿using DataBaseApi;
using InterfaceLibrary;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Win32;
using System;

namespace RestAPINext
{
    public class DataBase : DbContext
    {
        private string dataBaseName = string.Empty;
        private string dataBaseType = string.Empty;
        private string server = string.Empty;
        private string user = string.Empty;
        private string password = string.Empty;

        private DbSet<Sportsman> Sportsman { get; set; } = null!;
        private DbSet<Coach> Coach { get; set; } = null!;
        private DbSet<ClassType> ClassType { get; set; } = null!;
        private DbSet<Group> Group { get; set; } = null!;
        private DbSet<GroupMembers> GroupMembers { get; set; } = null!;
        private DbSet<RoomGym> RoomGym { get; set; } = null!;
        private DbSet<TimeTable> TimeTable { get; set; } = null!;
        private DbSet<User> UserTable { get; set; } = null!;

        public DataBase()
        {
            //this.dataBaseName = dataBaseName;
            readDataFromReg();
            Database.EnsureCreated();
        }

        private void readDataFromReg()
        {
            //путь в реестре Компьютер\HKEY_CURRENT_USER\Software\ABOBUS\ABOBUSAPI
            string regKey = "Software\\ABOBUS\\ABOBUSAPI";

            using (var key = Registry.CurrentUser.OpenSubKey(regKey, false))
            {
                dataBaseType = key?.GetValue("DataBaseType") as string;
                dataBaseName = key?.GetValue("DataBaseName") as string;
                server = key?.GetValue("Server") as string;
                password = key?.GetValue("Password") as string;
                user = key?.GetValue("User") as string;
            }
        }

        //создание таблиц в бд для маринки
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sportsman>().ToTable("Sportsman");
            modelBuilder.Entity<Coach>().ToTable("Coach");
            modelBuilder.Entity<ClassType>().ToTable("ClassType");
            modelBuilder.Entity<Group>().ToTable("Group");
            modelBuilder.Entity<GroupMembers>().ToTable("GroupMembers");
            modelBuilder.Entity<RoomGym>().ToTable("RoomGym");
            modelBuilder.Entity<TimeTable>().ToTable("TimeTable");
            modelBuilder.Entity<User>().ToTable("UserTable");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (dataBaseType == "SqLite")
            {
                optionsBuilder.UseSqlite($"Data Source={dataBaseName}.db");
            }
            else if (dataBaseType == "Maria")
            {
                string connectionString = $"server={server};user={user};password={password};database={dataBaseName}";

                MySqlServerVersion serverVersion = new MySqlServerVersion(new Version(10, 9, 8));

                optionsBuilder.UseMySql(connectionString, serverVersion);
            }

        }

    }
}
