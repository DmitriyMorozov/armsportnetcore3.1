﻿using DataBaseApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class RoomGymSheduleWindow : Form
    {
        private RestClient client;

        private List<TimeTable> loadedTimeTable;
        private List<RoomGym> loadedRoomGym;
        private List<Coach> loadedCoach;
        private List<Group> loadedGroup;

        private SortedDictionary<string, List<TimeTable>> sortedDataRooms =
            new SortedDictionary<string, List<TimeTable>>();

        public RoomGymSheduleWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            loadedCoach = client.Get<Coach>("api/Coach");
            loadedRoomGym = client.Get<RoomGym>("api/RoomGym");
            loadedGroup = client.Get<Group>("api/Group");
            loadedTimeTable = client.Get<TimeTable>("api/TimeTable");

            sortDataRooms();

            List<string> dictKeys = sortedDataRooms.Keys.ToList();
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("Зал", typeof(string)));
            dt.Columns.Add(new DataColumn("Дата", typeof(string)));
            dt.Columns.Add(new DataColumn("Время", typeof(string)));
            dt.Columns.Add(new DataColumn("Группа", typeof(string)));

            foreach (var item in sortedDataRooms)
            {
                foreach (var val in item.Value)
                {
                    DataRow row = dt.NewRow();

                    row["Зал"] = item.Key;
                    row["Дата"] = $"{val.Date.ToShortDateString()}";
                    row["Время"] = $"{val.TimeStart.ToShortTimeString()} - {val.TimeEnd.ToShortTimeString()}";
                    row["Группа"] = $"{loadedGroup.FirstOrDefault(g => g.Id == val.GroupId).Name}";

                    dt.Rows.Add(row);
                }
            }

            SheduleDataGrid.DataSource = dt;

            SheduleDataGrid.AutoResizeColumns();
            SheduleDataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }


        private void sortDataRooms()
        {
            foreach (var item in loadedTimeTable)
            {
                string roomName = loadedRoomGym.FirstOrDefault(r => r.Id == item.RoomGymId).Name;

                if (!sortedDataRooms.ContainsKey(roomName))
                {
                    sortedDataRooms[roomName] = new List<TimeTable>() { item };
                }
                else
                {
                    sortedDataRooms[roomName].Add(item);
                }
            }

            foreach (var item in sortedDataRooms)
            {
                if (item.Value.Count > 1)
                {
                    sortedDataRooms[item.Key].Sort((x, y) => x.TimeStart.CompareTo(y.TimeStart));
                }
            }
        }
    }
}
