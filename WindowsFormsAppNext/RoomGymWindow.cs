﻿using DataBaseApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class RoomGymWindow : Form
    {
        private RestClient client;
        private bool isEdit = false;
        private RoomGym? selectedRow;

        public RoomGymWindow(RestClient client, int id = -1)
        {
            InitializeComponent();
            this.client = client;

            if (id > 0)
            {
                selectedRow = client.Get<RoomGym>("api/RoomGym", id);
                isEdit = true;

                RoomTextBox.Text = selectedRow.Name;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            RoomGym room = new RoomGym();
            string strType = RoomTextBox.Text;

            if (strType == "")
            {
                MessageBox.Show("Поле \"Помещение\" должно быть заполнено!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!isEdit)
            {
                // Проверяем, не существует ли уже записи с таким именем
                var existingRoom = client.Get<RoomGym>("api/RoomGym").FirstOrDefault(x => x.Name == strType);

                if (existingRoom != null)
                {
                    MessageBox.Show("Поле \"Помещение\" должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                room = new RoomGym { Name = strType };
                client.Post("api/RoomGym", room);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                selectedRow.Name = strType;
                client.Put("api/RoomGym", selectedRow);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
