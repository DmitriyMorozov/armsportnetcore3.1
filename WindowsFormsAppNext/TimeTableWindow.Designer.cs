﻿using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class TimeTableWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DatePicker = new DateTimePicker();
            SaveButton = new Button();
            CancelButton = new Button();
            RoomGymDataGridView = new DataGridView();
            IdRoom = new DataGridViewTextBoxColumn();
            NameRoom = new DataGridViewTextBoxColumn();
            GroupDataGridView = new DataGridView();
            IdGroup = new DataGridViewTextBoxColumn();
            NameGroup = new DataGridViewTextBoxColumn();
            CoachDataGridView = new DataGridView();
            IdCoach = new DataGridViewTextBoxColumn();
            NameCoach = new DataGridViewTextBoxColumn();
            TimeStartPicker = new DateTimePicker();
            TimeEndPicker = new DateTimePicker();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            ((System.ComponentModel.ISupportInitialize)RoomGymDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)GroupDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)CoachDataGridView).BeginInit();
            SuspendLayout();
            // 
            // DatePicker
            // 
            DatePicker.Location = new System.Drawing.Point(22, 66);
            DatePicker.Margin = new Padding(6, 6, 6, 6);
            DatePicker.Name = "DatePicker";
            DatePicker.Size = new System.Drawing.Size(368, 39);
            DatePicker.TabIndex = 0;
            // 
            // SaveButton
            // 
            SaveButton.Location = new System.Drawing.Point(22, 823);
            SaveButton.Margin = new Padding(6, 6, 6, 6);
            SaveButton.Name = "SaveButton";
            SaveButton.Size = new System.Drawing.Size(139, 49);
            SaveButton.TabIndex = 1;
            SaveButton.Text = "Сохранить";
            SaveButton.UseVisualStyleBackColor = true;
            SaveButton.Click += SaveButton_Click;
            // 
            // CancelButton
            // 
            CancelButton.Location = new System.Drawing.Point(860, 823);
            CancelButton.Margin = new Padding(6, 6, 6, 6);
            CancelButton.Name = "CancelButton";
            CancelButton.Size = new System.Drawing.Size(139, 49);
            CancelButton.TabIndex = 2;
            CancelButton.Text = "Отмена";
            CancelButton.UseVisualStyleBackColor = true;
            CancelButton.Click += CancelButton_Click;
            // 
            // RoomGymDataGridView
            // 
            RoomGymDataGridView.AllowUserToAddRows = false;
            RoomGymDataGridView.AllowUserToDeleteRows = false;
            RoomGymDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            RoomGymDataGridView.Columns.AddRange(new DataGridViewColumn[] { IdRoom, NameRoom });
            RoomGymDataGridView.Location = new System.Drawing.Point(22, 164);
            RoomGymDataGridView.Margin = new Padding(6, 6, 6, 6);
            RoomGymDataGridView.Name = "RoomGymDataGridView";
            RoomGymDataGridView.ReadOnly = true;
            RoomGymDataGridView.RowHeadersWidth = 82;
            RoomGymDataGridView.RowTemplate.Height = 25;
            RoomGymDataGridView.Size = new System.Drawing.Size(453, 320);
            RoomGymDataGridView.TabIndex = 3;
            RoomGymDataGridView.CellClick += RoomGymDataGridView_CellContentClick;
            // 
            // IdRoom
            // 
            IdRoom.HeaderText = "Id";
            IdRoom.MinimumWidth = 10;
            IdRoom.Name = "IdRoom";
            IdRoom.ReadOnly = true;
            IdRoom.Width = 200;
            // 
            // NameRoom
            // 
            NameRoom.HeaderText = "Помещение";
            NameRoom.MinimumWidth = 10;
            NameRoom.Name = "NameRoom";
            NameRoom.ReadOnly = true;
            NameRoom.Width = 200;
            // 
            // GroupDataGridView
            // 
            GroupDataGridView.AllowUserToAddRows = false;
            GroupDataGridView.AllowUserToDeleteRows = false;
            GroupDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            GroupDataGridView.Columns.AddRange(new DataGridViewColumn[] { IdGroup, NameGroup });
            GroupDataGridView.Location = new System.Drawing.Point(553, 66);
            GroupDataGridView.Margin = new Padding(6, 6, 6, 6);
            GroupDataGridView.Name = "GroupDataGridView";
            GroupDataGridView.ReadOnly = true;
            GroupDataGridView.RowHeadersWidth = 82;
            GroupDataGridView.RowTemplate.Height = 25;
            GroupDataGridView.Size = new System.Drawing.Size(446, 320);
            GroupDataGridView.TabIndex = 4;
            GroupDataGridView.CellClick += GroupDataGridView_CellContentClick;
            // 
            // IdGroup
            // 
            IdGroup.HeaderText = "Id";
            IdGroup.MinimumWidth = 10;
            IdGroup.Name = "IdGroup";
            IdGroup.ReadOnly = true;
            IdGroup.Width = 200;
            // 
            // NameGroup
            // 
            NameGroup.HeaderText = "Группа";
            NameGroup.MinimumWidth = 10;
            NameGroup.Name = "NameGroup";
            NameGroup.ReadOnly = true;
            NameGroup.Width = 200;
            // 
            // CoachDataGridView
            // 
            CoachDataGridView.AllowUserToAddRows = false;
            CoachDataGridView.AllowUserToDeleteRows = false;
            CoachDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            CoachDataGridView.Columns.AddRange(new DataGridViewColumn[] { IdCoach, NameCoach });
            CoachDataGridView.Location = new System.Drawing.Point(553, 446);
            CoachDataGridView.Margin = new Padding(6, 6, 6, 6);
            CoachDataGridView.Name = "CoachDataGridView";
            CoachDataGridView.ReadOnly = true;
            CoachDataGridView.RowHeadersWidth = 82;
            CoachDataGridView.RowTemplate.Height = 25;
            CoachDataGridView.Size = new System.Drawing.Size(446, 320);
            CoachDataGridView.TabIndex = 5;
            CoachDataGridView.CellClick += CoachDataGridView_CellContentClick;
            // 
            // IdCoach
            // 
            IdCoach.HeaderText = "Id";
            IdCoach.MinimumWidth = 10;
            IdCoach.Name = "IdCoach";
            IdCoach.ReadOnly = true;
            IdCoach.Width = 200;
            // 
            // NameCoach
            // 
            NameCoach.HeaderText = "ФИО тренера";
            NameCoach.MinimumWidth = 10;
            NameCoach.Name = "NameCoach";
            NameCoach.ReadOnly = true;
            NameCoach.Width = 200;
            // 
            // TimeStartPicker
            // 
            TimeStartPicker.Location = new System.Drawing.Point(22, 623);
            TimeStartPicker.Margin = new Padding(6, 6, 6, 6);
            TimeStartPicker.Name = "TimeStartPicker";
            TimeStartPicker.Size = new System.Drawing.Size(368, 39);
            TimeStartPicker.TabIndex = 6;
            // 
            // TimeEndPicker
            // 
            TimeEndPicker.Location = new System.Drawing.Point(22, 717);
            TimeEndPicker.Margin = new Padding(6, 6, 6, 6);
            TimeEndPicker.Name = "TimeEndPicker";
            TimeEndPicker.Size = new System.Drawing.Size(368, 39);
            TimeEndPicker.TabIndex = 7;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(22, 28);
            label1.Margin = new Padding(6, 0, 6, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(65, 32);
            label1.TabIndex = 8;
            label1.Text = "Дата";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(22, 126);
            label2.Margin = new Padding(6, 0, 6, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(149, 32);
            label2.TabIndex = 9;
            label2.Text = "Помещение";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(22, 585);
            label3.Margin = new Padding(6, 0, 6, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(168, 32);
            label3.TabIndex = 10;
            label3.Text = "Время начала";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(22, 678);
            label4.Margin = new Padding(6, 0, 6, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(157, 32);
            label4.TabIndex = 11;
            label4.Text = "Время конца";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(553, 28);
            label5.Margin = new Padding(6, 0, 6, 0);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(91, 32);
            label5.TabIndex = 12;
            label5.Text = "Группа";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(553, 407);
            label6.Margin = new Padding(6, 0, 6, 0);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(95, 32);
            label6.TabIndex = 13;
            label6.Text = "Тренер";
            // 
            // TimeTableWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1010, 896);
            Controls.Add(label6);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(TimeEndPicker);
            Controls.Add(TimeStartPicker);
            Controls.Add(CoachDataGridView);
            Controls.Add(GroupDataGridView);
            Controls.Add(RoomGymDataGridView);
            Controls.Add(CancelButton);
            Controls.Add(SaveButton);
            Controls.Add(DatePicker);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Margin = new Padding(6, 6, 6, 6);
            Name = "TimeTableWindow";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Расписание";
            ((System.ComponentModel.ISupportInitialize)RoomGymDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)GroupDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)CoachDataGridView).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DateTimePicker DatePicker;
        private Button SaveButton;
        private Button CancelButton;
        private DataGridView RoomGymDataGridView;
        private DataGridView GroupDataGridView;
        private DataGridView CoachDataGridView;
        private DateTimePicker TimeStartPicker;
        private DateTimePicker TimeEndPicker;
        private DataGridViewTextBoxColumn IdRoom;
        private DataGridViewTextBoxColumn NameRoom;
        private DataGridViewTextBoxColumn IdGroup;
        private DataGridViewTextBoxColumn NameGroup;
        private DataGridViewTextBoxColumn IdCoach;
        private DataGridViewTextBoxColumn NameCoach;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
    }
}