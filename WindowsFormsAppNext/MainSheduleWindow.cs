﻿using DataBaseApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class MainSheduleWindow : Form
    {
        private RestClient client;

        private SortedDictionary<string, List<TimeTable>> sortedData =
            new SortedDictionary<string, List<TimeTable>>();

        private List<TimeTable> loadedTimeTable;
        private List<RoomGym> loadedRoomGym;
        private List<Group> loadedGroup;

        public MainSheduleWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            loadedRoomGym = client.Get<RoomGym>("api/RoomGym");
            loadedGroup = client.Get<Group>("api/Group");
            loadedTimeTable = client.Get<TimeTable>("api/TimeTable");

            sortData();

            showMainShedule();
        }

        private void showMainShedule()
        {
            List<string> dictKeys = sortedData.Keys.ToList();
            DataTable dt = new DataTable();

            // Создайте столбцы для таблицы
            dt.Columns.Add(new DataColumn("Дата", typeof(string)));
            dt.Columns.Add(new DataColumn("Время", typeof(string)));
            dt.Columns.Add(new DataColumn("Группа", typeof(string)));
            dt.Columns.Add(new DataColumn("Зал", typeof(string)));

            // Заполните таблицу данными
            for (int i = 0; i < loadedTimeTable.Count; i++)
            {
                DataRow row = dt.NewRow();
                var item = loadedTimeTable[i];

                string dateKey = item.Date.ToShortDateString();

                // Заполните ячейку для даты
                row["Дата"] = dateKey;

                // Заполните ячейку для времени
                string timeValue = $"{item.TimeStart.ToShortTimeString()} - {item.TimeEnd.ToShortTimeString()}";
                row["Время"] = timeValue;

                // Заполните ячейку для группы
                string groupName = loadedGroup.FirstOrDefault(g => g.Id == item.GroupId).Name;
                row["Группа"] = groupName;

                // Заполните ячейку для зала
                string roomName = loadedRoomGym.FirstOrDefault(r => r.Id == item.RoomGymId).Name;
                row["Зал"] = roomName;

                dt.Rows.Add(row);
            }

            SheduleDataGrid.DataSource = dt;

            // Установите AutoSizeColumnsMode для каждой колонки
            foreach (DataGridViewColumn column in SheduleDataGrid.Columns)
            {
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void sortData()
        {
            foreach (var item in loadedTimeTable)
            {
                string shortDate = item.Date.ToShortDateString();

                if (!sortedData.ContainsKey(shortDate))
                {
                    sortedData[shortDate] = new List<TimeTable>() { item };
                }
                else
                {
                    sortedData[shortDate].Add(item);
                }
            }

            foreach (var item in sortedData)
            {
                if (item.Value.Count > 1)
                {
                    sortedData[item.Key].Sort((x, y) => x.TimeStart.CompareTo(y.TimeStart));
                }
            }
        }
    }
}
