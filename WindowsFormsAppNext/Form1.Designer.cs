﻿using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            authButton = new Button();
            label1 = new Label();
            label2 = new Label();
            LoginTextBox = new TextBox();
            PasswordTextBox = new TextBox();
            contextMenuStrip1 = new ContextMenuStrip(components);
            regButton = new Button();
            SuspendLayout();
            // 
            // authButton
            // 
            authButton.Location = new System.Drawing.Point(33, 118);
            authButton.Margin = new Padding(2, 1, 2, 1);
            authButton.Name = "authButton";
            authButton.Size = new System.Drawing.Size(109, 22);
            authButton.TabIndex = 0;
            authButton.Text = "Авторизация";
            authButton.UseVisualStyleBackColor = true;
            authButton.Click += authButton_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(39, 27);
            label1.Margin = new Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(41, 15);
            label1.TabIndex = 1;
            label1.Text = "Логин";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(39, 71);
            label2.Margin = new Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(49, 15);
            label2.TabIndex = 2;
            label2.Text = "Пароль";
            // 
            // LoginTextBox
            // 
            LoginTextBox.Location = new System.Drawing.Point(34, 43);
            LoginTextBox.Margin = new Padding(2, 1, 2, 1);
            LoginTextBox.Name = "LoginTextBox";
            LoginTextBox.Size = new System.Drawing.Size(110, 23);
            LoginTextBox.TabIndex = 3;
            // 
            // PasswordTextBox
            // 
            PasswordTextBox.Location = new System.Drawing.Point(34, 88);
            PasswordTextBox.Margin = new Padding(2, 1, 2, 1);
            PasswordTextBox.Name = "PasswordTextBox";
            PasswordTextBox.PasswordChar = '*';
            PasswordTextBox.Size = new System.Drawing.Size(110, 23);
            PasswordTextBox.TabIndex = 4;
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // regButton
            // 
            regButton.Location = new System.Drawing.Point(37, 151);
            regButton.Margin = new Padding(2, 1, 2, 1);
            regButton.Name = "regButton";
            regButton.Size = new System.Drawing.Size(104, 22);
            regButton.TabIndex = 5;
            regButton.Text = "Регистрация";
            regButton.UseVisualStyleBackColor = true;
            regButton.Click += regButton_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(200, 278);
            Controls.Add(regButton);
            Controls.Add(PasswordTextBox);
            Controls.Add(LoginTextBox);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(authButton);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Margin = new Padding(2, 1, 2, 1);
            Name = "Form1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Вход";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button authButton;
        private Label label1;
        private Label label2;
        private TextBox LoginTextBox;
        private TextBox PasswordTextBox;
        private ContextMenuStrip contextMenuStrip1;
        private Button regButton;
    }
}
