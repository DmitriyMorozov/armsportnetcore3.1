﻿using DataBaseApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class Form1 : Form
    {
        public RestClient client;

        public Form1()
        {
            InitializeComponent();
            client = new RestClient("http://localhost:5000/");
        }


        private void authButton_Click(object sender, EventArgs e)
        {
            try
            {
                User user = new User() { Login = LoginTextBox.Text, Password = PasswordTextBox.Text };


                client.Post("auth/Login", user);

                if (client.IsOk())
                {
                    MessageBox.Show("Пользователь успешно авторизован!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    string token = client.GetContent();
                    client.SetToken(token);

                    MainWindow window = new MainWindow(client);
                    window.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Неверно указан логин или пароль!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ошибка: Сервер недоступен. Проверьте соединение с интернетом или адрес сервера.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void regButton_Click(object sender, EventArgs e)
        {
            try
            {
                User user = new User() { Login = LoginTextBox.Text, Password = PasswordTextBox.Text };


                client.Post("auth/Register", user);

                if (client.IsOk())
                {
                    MessageBox.Show("Пользователь успешно зарегистрирован!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    string token = client.GetContent();
                    client.SetToken(token);
                }
                else
                {
                    MessageBox.Show("Неверно указан логин или пароль!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ошибка: Сервер недоступен. Проверьте соединение с интернетом или адрес сервера.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
