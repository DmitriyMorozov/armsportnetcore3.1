﻿using DataBaseApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class MainWindow : Form
    {
        private RestClient client;
        private string[] tableNames =
        {
            "Sportsman",
            "Coach",
            "ClassType",
            "RoomGym",
            "Group",
            "GroupMembers",
            "TimeTable"
        };
        private string selectedTable = string.Empty;

        public MainWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            addButton.Visible = false;
            deleteButton.Visible = false;
            editButton.Visible = false;

        }

        private void updateGrid(string tableName)
        {
            dataGridView.DataSource = null;
            dataGridView.Columns.Clear();
            dataGridView.Refresh();

            DataTable dt = new DataTable();

            switch (tableName)
            {
                case "Sportsman":
                    var loadedData = client.Get<Sportsman>("api/Sportsman");

                    dt.Columns.Add(new DataColumn("Id", typeof(int)));
                    dt.Columns.Add(new DataColumn("ФИО", typeof(string)));
                    dt.Columns.Add(new DataColumn("ИНН", typeof(string)));
                    dt.Columns.Add(new DataColumn("Моб. номер", typeof(string)));
                    dt.Columns.Add(new DataColumn("Дата рождения", typeof(DateTime)));

                    if (loadedData != null)
                    {
                        foreach (var row in loadedData)
                        {
                            dt.Rows.Add(new object[] {
                            row.Id,
                            row.FIO,
                            row.INN,
                            row.PhoneNumber,
                            row.BirthDate
                        });
                        }
                    }

                    dataGridView.DataSource = dt;

                    dataGridView.Columns["Id"].Visible = false;
                    dataGridView.Columns["Дата рождения"].DefaultCellStyle.Format = "dd.MM.yyyy";
                    break;
                case "Coach":
                    var loadedDataCoach = client.Get<Coach>("api/Coach");

                    dt.Columns.Add(new DataColumn("Id", typeof(int)));
                    dt.Columns.Add(new DataColumn("ФИО", typeof(string)));
                    dt.Columns.Add(new DataColumn("ИНН", typeof(string)));
                    dt.Columns.Add(new DataColumn("Моб. номер", typeof(string)));
                    dt.Columns.Add(new DataColumn("Дата рождения", typeof(DateTime)));

                    if (loadedDataCoach != null)
                    {
                        foreach (var row in loadedDataCoach)
                        {
                            dt.Rows.Add(new object[] {
                            row.Id,
                            row.FIO,
                            row.INN,
                            row.PhoneNumber,
                            row.BirthDate
                        });
                        }
                    }

                    dataGridView.DataSource = dt;

                    dataGridView.Columns["Id"].Visible = false;
                    dataGridView.Columns["Дата рождения"].DefaultCellStyle.Format = "dd.MM.yyyy";
                    break;
                case "ClassType":
                    var loadedDataClassType = client.Get<ClassType>("api/ClassType");

                    dt.Columns.Add(new DataColumn("Id", typeof(int)));
                    dt.Columns.Add(new DataColumn("Вид спорта", typeof(string)));

                    if (loadedDataClassType != null)
                    {
                        foreach (var row in loadedDataClassType)
                        {
                            dt.Rows.Add(new object[] {
                            row.Id,
                            row.Name
                        });
                        }
                    }

                    dataGridView.DataSource = dt;

                    dataGridView.Columns["Id"].Visible = false;
                    break;
                case "RoomGym":
                    var loadedDataRoomGym = client.Get<RoomGym>("api/RoomGym");

                    dt.Columns.Add(new DataColumn("Id", typeof(int)));
                    dt.Columns.Add(new DataColumn("Помещение", typeof(string)));

                    if (loadedDataRoomGym != null)
                    {
                        foreach (var row in loadedDataRoomGym)
                        {
                            dt.Rows.Add(new object[] {
                            row.Id,
                            row.Name
                        });
                        }
                    }

                    dataGridView.DataSource = dt;

                    dataGridView.Columns["Id"].Visible = false;
                    break;
                case "Group":
                    var loadedDataGroup = client.Get<Group>("api/Group");

                    dt.Columns.Add(new DataColumn("Id", typeof(int)));
                    dt.Columns.Add(new DataColumn("Название группы", typeof(string)));
                    dt.Columns.Add(new DataColumn("Вид спорта", typeof(string)));

                    if (loadedDataGroup != null)
                    {
                        foreach (var row in loadedDataGroup)
                        {
                            dt.Rows.Add(new object[] {
                            row.Id,
                            row.Name,
                            client.Get<ClassType>("api/ClassType", row.ClassTypeId).Name
                        });
                        }
                    }

                    dataGridView.DataSource = dt;

                    dataGridView.Columns["Id"].Visible = false;
                    break;
                case "GroupMembers":
                    var loadedDataGroupMembers = client.Get<GroupMembers>("api/GroupMembers");

                    dt.Columns.Add(new DataColumn("Id", typeof(int)));
                    dt.Columns.Add(new DataColumn("Спортсмен", typeof(string)));
                    dt.Columns.Add(new DataColumn("Группа", typeof(string)));

                    if (loadedDataGroupMembers != null)
                    {
                        foreach (var row in loadedDataGroupMembers)
                        {
                            dt.Rows.Add(new object[] {
                            row.Id,
                            client.Get<Sportsman>("api/Sportsman", row.SportsmanId).FIO,
                            client.Get<Group>("api/Group", row.GroupId).Name,
                        });
                        }
                    }

                    dataGridView.DataSource = dt;

                    dataGridView.Columns["Id"].Visible = false;
                    break;
                case "TimeTable":
                    var loadedDataTimeTable = client.Get<TimeTable>("api/TimeTable");

                    dt.Columns.Add(new DataColumn("Id", typeof(int)));
                    dt.Columns.Add(new DataColumn("Дата занятия", typeof(DateTime)));
                    dt.Columns.Add(new DataColumn("Помещение", typeof(string)));
                    dt.Columns.Add(new DataColumn("Время начала", typeof(DateTime)));
                    dt.Columns.Add(new DataColumn("Время конца", typeof(DateTime)));
                    dt.Columns.Add(new DataColumn("Тренер", typeof(string)));
                    dt.Columns.Add(new DataColumn("Группа", typeof(string)));

                    if (loadedDataTimeTable != null)
                    {
                        foreach (var row in loadedDataTimeTable)
                        {
                            dt.Rows.Add(new object[] {
                            row.Id,
                            row.Date,
                            client.Get<RoomGym>("api/RoomGym", row.RoomGymId).Name,
                            row.TimeStart,
                            row.TimeEnd,
                            client.Get<Coach>("api/Coach", row.CoachId).FIO,
                            client.Get<Group>("api/Group", row.GroupId).Name
                        });
                        }
                    }

                    dataGridView.DataSource = dt;

                    dataGridView.Columns["Дата занятия"].DefaultCellStyle.Format = "dd.MM.yyyy";
                    dataGridView.Columns["Время начала"].DefaultCellStyle.Format = "HH:mm";
                    dataGridView.Columns["Время конца"].DefaultCellStyle.Format = "HH:mm";
                    dataGridView.Columns["Id"].Visible = false;
                    break;
                default:
                    break;
            }

            addButton.Visible = true;
            deleteButton.Visible = true;
            editButton.Visible = true;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            switch (selectedTable)
            {
                case "Sportsman":
                    SportsmanWindow windowSportsman = new SportsmanWindow(client);
                    windowSportsman.ShowDialog();
                    break;
                case "Coach":
                    CoachWindow windowCoach = new CoachWindow(client);
                    windowCoach.ShowDialog();
                    break;
                case "ClassType":
                    ClassTypeWindow windowClassType = new ClassTypeWindow(client);
                    windowClassType.ShowDialog();
                    break;
                case "RoomGym":
                    RoomGymWindow windowRoomGym = new RoomGymWindow(client);
                    windowRoomGym.ShowDialog();
                    break;
                case "Group":
                    GroupWindow windowGroup = new GroupWindow(client);
                    windowGroup.ShowDialog();
                    break;
                case "GroupMembers":
                    GroupMembersWindow windowGroupMembers = new GroupMembersWindow(client);
                    windowGroupMembers.ShowDialog();
                    break;
                case "TimeTable":
                    TimeTableWindow windowTimeTable = new TimeTableWindow(client);
                    windowTimeTable.ShowDialog();
                    break;
                default:
                    break;
            }

            updateGrid(selectedTable);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            int selectedIndex = dataGridView.SelectedRows[0].Index;

            int id = (int)dataGridView.Rows[selectedIndex].Cells[0].Value;

            ///////////////////////////////////////////

            bool isOk = true;

            if (selectedTable == tableNames[0])
            {
                if (client.Get<GroupMembers>("api/GroupMembers").
                    FirstOrDefault(x => x.SportsmanId == id) != null)
                {
                    isOk = false;
                }
            }
            else if (selectedTable == tableNames[1])
            {
                if (client.Get<TimeTable>("api/TimeTable").
                    FirstOrDefault(x => x.CoachId == id) != null)
                {
                    isOk = false;
                }
            }
            else if (selectedTable == tableNames[2])
            {
                if (client.Get<Group>("api/Group").
                    FirstOrDefault(x => x.ClassTypeId == id) != null)
                {
                    isOk = false;
                }
            }
            else if (selectedTable == tableNames[3])
            {
                if (client.Get<TimeTable>("api/TimeTable").
                    FirstOrDefault(x => x.RoomGymId == id) != null)
                {
                    isOk = false;
                }
            }
            else if (selectedTable == tableNames[4])
            {
                if (client.Get<GroupMembers>("api/GroupMembers").
                    FirstOrDefault(x => x.GroupId == id) != null)
                {
                    isOk = false;
                }
            }


            if (!isOk)
            {
                MessageBox.Show("Нельзя удалить запись\nЗапись исользуется в других таблицах!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                client.Delete($"api/{selectedTable}", id);

                if (client.IsOk())
                {
                    MessageBox.Show("Запись успешно удалена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView.Rows.RemoveAt(selectedIndex);
                }
                else
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                    MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            int selectedIndex = dataGridView.SelectedRows[0].Index;

            int id = (int)dataGridView.Rows[selectedIndex].Cells[0].Value;

            switch (selectedTable)
            {
                case "Sportsman":
                    SportsmanWindow window = new SportsmanWindow(client, id);
                    window.ShowDialog();
                    break;
                case "Coach":
                    CoachWindow windowCoach = new CoachWindow(client, id);
                    windowCoach.ShowDialog();
                    break;
                case "ClassType":
                    ClassTypeWindow windowClassType = new ClassTypeWindow(client, id);
                    windowClassType.ShowDialog();
                    break;
                case "RoomGym":
                    RoomGymWindow windowRoomGym = new RoomGymWindow(client, id);
                    windowRoomGym.ShowDialog();
                    break;
                case "Group":
                    GroupWindow windowGroup = new GroupWindow(client, id);
                    windowGroup.ShowDialog();
                    break;
                case "GroupMembers":
                    GroupMembersWindow windowGroupMembers = new GroupMembersWindow(client, id);
                    windowGroupMembers.ShowDialog();
                    break;
                case "TimeTable":
                    TimeTableWindow windowTimeTable = new TimeTableWindow(client, id);
                    windowTimeTable.ShowDialog();
                    break;
                default:
                    break;
            }

            updateGrid(selectedTable);
        }

        private void sportsmanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedTable = tableNames[0];

            updateGrid(selectedTable);
        }

        private void coachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedTable = tableNames[1];

            updateGrid(selectedTable);
        }

        private void classTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedTable = tableNames[2];

            updateGrid(selectedTable);
        }

        private void roomGymToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedTable = tableNames[3];

            updateGrid(selectedTable);
        }

        private void GroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedTable = tableNames[4];

            updateGrid(selectedTable);
        }

        private void GroupMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedTable = tableNames[5];

            updateGrid(selectedTable);
        }

        private void TimeTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectedTable = tableNames[6];

            updateGrid(selectedTable);
        }

        private void MainSheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainSheduleWindow window = new MainSheduleWindow(client);
            window.ShowDialog();
        }

        private void CoachSheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CoachSheduleWindow window = new CoachSheduleWindow(client);
            window.ShowDialog();
        }

        private void RoomGymSheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RoomGymSheduleWindow window = new RoomGymSheduleWindow(client);
            window.ShowDialog();
        }
    }
}
