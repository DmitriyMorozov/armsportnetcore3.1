﻿using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class MainSheduleWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            SheduleDataGrid = new DataGridView();
            ((System.ComponentModel.ISupportInitialize)SheduleDataGrid).BeginInit();
            SuspendLayout();
            // 
            // SheduleDataGrid
            // 
            SheduleDataGrid.AllowUserToAddRows = false;
            SheduleDataGrid.AllowUserToDeleteRows = false;
            SheduleDataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            SheduleDataGrid.Location = new System.Drawing.Point(12, 7);
            SheduleDataGrid.Name = "SheduleDataGrid";
            SheduleDataGrid.ReadOnly = true;
            SheduleDataGrid.RowTemplate.Height = 25;
            SheduleDataGrid.Size = new System.Drawing.Size(572, 331);
            SheduleDataGrid.TabIndex = 0;
            // 
            // MainSheduleWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(596, 350);
            Controls.Add(SheduleDataGrid);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Name = "MainSheduleWindow";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Расписание";
            ((System.ComponentModel.ISupportInitialize)SheduleDataGrid).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView SheduleDataGrid;
    }
}