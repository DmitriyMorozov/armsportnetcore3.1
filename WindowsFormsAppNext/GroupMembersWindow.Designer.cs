﻿using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class GroupMembersWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            SaveButton = new Button();
            CancelButton = new Button();
            SportDataGridView = new DataGridView();
            Id = new DataGridViewTextBoxColumn();
            FIO = new DataGridViewTextBoxColumn();
            GroupDataGridView = new DataGridView();
            IdGroup = new DataGridViewTextBoxColumn();
            NameGroup = new DataGridViewTextBoxColumn();
            label1 = new Label();
            label2 = new Label();
            ((System.ComponentModel.ISupportInitialize)SportDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)GroupDataGridView).BeginInit();
            SuspendLayout();
            // 
            // SaveButton
            // 
            SaveButton.Location = new System.Drawing.Point(12, 215);
            SaveButton.Name = "SaveButton";
            SaveButton.Size = new System.Drawing.Size(75, 23);
            SaveButton.TabIndex = 0;
            SaveButton.Text = "Сохранить";
            SaveButton.UseVisualStyleBackColor = true;
            SaveButton.Click += SaveButton_Click;
            // 
            // CancelButton
            // 
            CancelButton.Location = new System.Drawing.Point(419, 215);
            CancelButton.Name = "CancelButton";
            CancelButton.Size = new System.Drawing.Size(75, 23);
            CancelButton.TabIndex = 1;
            CancelButton.Text = "Отмена";
            CancelButton.UseVisualStyleBackColor = true;
            CancelButton.Click += CancelButton_Click;
            // 
            // SportDataGridView
            // 
            SportDataGridView.AllowUserToAddRows = false;
            SportDataGridView.AllowUserToDeleteRows = false;
            SportDataGridView.AllowUserToResizeColumns = false;
            SportDataGridView.AllowUserToResizeRows = false;
            SportDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            SportDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            SportDataGridView.Columns.AddRange(new DataGridViewColumn[] { Id, FIO });
            SportDataGridView.Location = new System.Drawing.Point(12, 26);
            SportDataGridView.MultiSelect = false;
            SportDataGridView.Name = "SportDataGridView";
            SportDataGridView.RowTemplate.Height = 25;
            SportDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            SportDataGridView.Size = new System.Drawing.Size(216, 150);
            SportDataGridView.TabIndex = 2;
            SportDataGridView.CellClick += SportDataGridView_CellContentClick;
            // 
            // Id
            // 
            Id.HeaderText = "Id";
            Id.Name = "Id";
            // 
            // FIO
            // 
            FIO.HeaderText = "ФИО";
            FIO.Name = "FIO";
            // 
            // GroupDataGridView
            // 
            GroupDataGridView.AllowUserToAddRows = false;
            GroupDataGridView.AllowUserToDeleteRows = false;
            GroupDataGridView.AllowUserToResizeColumns = false;
            GroupDataGridView.AllowUserToResizeRows = false;
            GroupDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            GroupDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            GroupDataGridView.Columns.AddRange(new DataGridViewColumn[] { IdGroup, NameGroup });
            GroupDataGridView.Location = new System.Drawing.Point(268, 26);
            GroupDataGridView.MultiSelect = false;
            GroupDataGridView.Name = "GroupDataGridView";
            GroupDataGridView.RowTemplate.Height = 25;
            GroupDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GroupDataGridView.Size = new System.Drawing.Size(224, 150);
            GroupDataGridView.TabIndex = 3;
            GroupDataGridView.CellClick += GroupDataGridView_CellContentClick;
            // 
            // IdGroup
            // 
            IdGroup.HeaderText = "Id";
            IdGroup.Name = "IdGroup";
            // 
            // NameGroup
            // 
            NameGroup.HeaderText = "Имя";
            NameGroup.Name = "NameGroup";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(12, 8);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(78, 15);
            label1.TabIndex = 4;
            label1.Text = "Спортсмены";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(270, 8);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(49, 15);
            label2.TabIndex = 5;
            label2.Text = "Группы";
            // 
            // GroupMembersWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(504, 258);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(GroupDataGridView);
            Controls.Add(SportDataGridView);
            Controls.Add(CancelButton);
            Controls.Add(SaveButton);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Name = "GroupMembersWindow";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "СоставГруппы";
            ((System.ComponentModel.ISupportInitialize)SportDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)GroupDataGridView).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button SaveButton;
        private Button CancelButton;
        private DataGridView SportDataGridView;
        private DataGridView GroupDataGridView;
        private Label label1;
        private Label label2;
        private DataGridViewTextBoxColumn Id;
        private DataGridViewTextBoxColumn FIO;
        private DataGridViewTextBoxColumn IdGroup;
        private DataGridViewTextBoxColumn NameGroup;
    }
}