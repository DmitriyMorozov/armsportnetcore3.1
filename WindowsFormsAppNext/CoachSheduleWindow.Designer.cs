﻿using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class CoachSheduleWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            CoachSheduleDataGrid = new DataGridView();
            CoachDataGridView = new DataGridView();
            IdCoach = new DataGridViewTextBoxColumn();
            FIO = new DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)CoachSheduleDataGrid).BeginInit();
            ((System.ComponentModel.ISupportInitialize)CoachDataGridView).BeginInit();
            SuspendLayout();
            // 
            // CoachSheduleDataGrid
            // 
            CoachSheduleDataGrid.AllowUserToAddRows = false;
            CoachSheduleDataGrid.AllowUserToDeleteRows = false;
            CoachSheduleDataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            CoachSheduleDataGrid.Location = new System.Drawing.Point(238, 12);
            CoachSheduleDataGrid.Name = "CoachSheduleDataGrid";
            CoachSheduleDataGrid.ReadOnly = true;
            CoachSheduleDataGrid.RowTemplate.Height = 25;
            CoachSheduleDataGrid.Size = new System.Drawing.Size(328, 345);
            CoachSheduleDataGrid.TabIndex = 0;
            // 
            // CoachDataGridView
            // 
            CoachDataGridView.AllowUserToAddRows = false;
            CoachDataGridView.AllowUserToDeleteRows = false;
            CoachDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            CoachDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            CoachDataGridView.Columns.AddRange(new DataGridViewColumn[] { IdCoach, FIO });
            CoachDataGridView.Location = new System.Drawing.Point(12, 12);
            CoachDataGridView.MultiSelect = false;
            CoachDataGridView.Name = "CoachDataGridView";
            CoachDataGridView.ReadOnly = true;
            CoachDataGridView.RowTemplate.Height = 25;
            CoachDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            CoachDataGridView.Size = new System.Drawing.Size(220, 345);
            CoachDataGridView.TabIndex = 1;
            CoachDataGridView.CellClick += CoachDataGridView_CellContentClick;
            // 
            // IdCoach
            // 
            IdCoach.HeaderText = "Id";
            IdCoach.Name = "IdCoach";
            IdCoach.ReadOnly = true;
            // 
            // FIO
            // 
            FIO.HeaderText = "ФИО";
            FIO.Name = "FIO";
            FIO.ReadOnly = true;
            // 
            // CoachSheduleWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(578, 369);
            Controls.Add(CoachDataGridView);
            Controls.Add(CoachSheduleDataGrid);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Name = "CoachSheduleWindow";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Расписание тренеров";
            ((System.ComponentModel.ISupportInitialize)CoachSheduleDataGrid).EndInit();
            ((System.ComponentModel.ISupportInitialize)CoachDataGridView).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView CoachSheduleDataGrid;
        private DataGridView CoachDataGridView;
        private DataGridViewTextBoxColumn IdCoach;
        private DataGridViewTextBoxColumn FIO;
    }
}