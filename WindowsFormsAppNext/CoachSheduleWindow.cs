﻿using DataBaseApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class CoachSheduleWindow : Form
    {
        private RestClient client;
        private int CoachId = -1;

        private SortedDictionary<string, List<TimeTable>> sortedDataCoaches =
            new SortedDictionary<string, List<TimeTable>>();

        private List<TimeTable> loadedTimeTable;
        private List<RoomGym> loadedRoomGym;
        private List<Coach> loadedCoach;
        private List<Group> loadedGroup;

        public CoachSheduleWindow(RestClient client)
        {
            InitializeComponent();
            this.client = client;

            loadedCoach = client.Get<Coach>("api/Coach");
            loadedRoomGym = client.Get<RoomGym>("api/RoomGym");
            loadedGroup = client.Get<Group>("api/Group");
            loadedTimeTable = client.Get<TimeTable>("api/TimeTable");

            foreach (var row in loadedCoach)
            {
                CoachDataGridView.Rows.Add(new object[]
                {
                    row.Id,
                    row.FIO
                });
            }

            CoachDataGridView.Columns["IdCoach"].Visible = false;
        }

        private void sortDataCoach()
        {
            sortedDataCoaches = new SortedDictionary<string, List<TimeTable>>();

            foreach (var item in loadedTimeTable)
            {
                Coach currentCoach = loadedCoach.FirstOrDefault(c => c.Id == item.CoachId);

                if (currentCoach.Id == CoachId)
                {
                    string shortDate = item.Date.ToShortDateString();

                    if (!sortedDataCoaches.ContainsKey(shortDate))
                    {
                        sortedDataCoaches[shortDate] = new List<TimeTable>() { item };
                    }
                    else
                    {
                        sortedDataCoaches[shortDate].Add(item);
                    }
                }
            }

            foreach (var item in sortedDataCoaches)
            {
                if (item.Value.Count > 1)
                {
                    sortedDataCoaches[item.Key].Sort((x, y) => x.TimeStart.CompareTo(y.TimeStart));
                }
            }
        }

        private void CoachDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            CoachSheduleDataGrid.DataSource = null;
            CoachSheduleDataGrid.Columns.Clear();
            CoachSheduleDataGrid.Rows.Clear();

            if (e.RowIndex < 0)
            {
                return;
            }

            int index = e.RowIndex;

            CoachId = (int)CoachDataGridView.Rows[index].Cells[0].Value;

            sortDataCoach();

            List<string> dictKeys = sortedDataCoaches.Keys.ToList();
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("Дата", typeof(string)));
            dt.Columns.Add(new DataColumn("Время", typeof(string)));
            dt.Columns.Add(new DataColumn("Группа", typeof(string)));
            dt.Columns.Add(new DataColumn("Зал", typeof(string)));

            foreach (var item in sortedDataCoaches)
            {
                foreach (var val in item.Value)
                {
                    DataRow row = dt.NewRow();

                    row["Дата"] = $"{val.Date.ToShortDateString()}";
                    row["Время"] = $"{val.TimeStart.ToShortTimeString()} - {val.TimeEnd.ToShortTimeString()}";
                    row["Группа"] = $"{loadedGroup.FirstOrDefault(g => g.Id == val.GroupId).Name}";
                    row["Зал"] = $"{loadedRoomGym.FirstOrDefault(g => g.Id == val.RoomGymId).Name}";

                    dt.Rows.Add(row);
                }
            }

            CoachSheduleDataGrid.DataSource = dt;

            CoachSheduleDataGrid.AutoResizeColumns();
            CoachSheduleDataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
    }
}
