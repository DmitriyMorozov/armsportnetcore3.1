﻿using DataBaseApi;
using InterfaceLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class GroupWindow : Form
    {
        private bool isEdit = false;
        private Group? selectedRow;
        private RestClient client;
        private int selectedRowType = -1;

        public GroupWindow(RestClient client, int id = -1)
        {
            InitializeComponent();
            this.client = client;

            var loadClassTypeData = client.Get<ClassType>("api/ClassType");

            foreach (var row in loadClassTypeData)
            {
                dataGridView1.Rows.Add(new object[]
                {
                    row.Id,
                    row.Name
                });
            }

            dataGridView1.Columns["Id"].Visible = false;

            if (id > 0)
            {
                selectedRow = client.Get<Group>("api/Group", id);
                isEdit = true;

                GroupTextBox.Text = selectedRow.Name;

                int count = 0;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if ((int)row.Cells["Id"].Value == selectedRow.ClassTypeId)
                    {
                        dataGridView1.CurrentCell = dataGridView1.Rows[count].Cells[1];
                        break;
                    }

                    count++;
                }
            }
        }

        public bool checkUnique(string Name)
        {
            // Если это редактирование, то проверка уникальности должна проходить,
            // исключая выбранный элемент (selectedRow)
            if (isEdit)
            {
                return !(client.Get<Group>("api/Group").FirstOrDefault(x => x.Name == Name && x.Id != selectedRow.Id) != null);
            }
            else
            {
                return !(client.Get<Group>("api/Group").FirstOrDefault(x => x.Name == Name) != null);
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Group group = new Group();
            string strType = GroupTextBox.Text;

            if (strType == "")
            {
                MessageBox.Show("Поле \"Название группы\" должно быть заполнено!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (selectedRowType != -1)
            {
                if (!isEdit)
                {
                    group = new Group { Name = strType, ClassTypeId = selectedRowType };

                    if (checkUnique(strType))
                    {
                        client.Post("api/Group", group);
                    }
                    else
                    {
                        MessageBox.Show("Поле \"Название группы\" должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    group = new Group { Id = selectedRow.Id, Name = strType, ClassTypeId = selectedRowType };

                    if (checkUnique(strType))
                    {
                        client.Put("api/Group", group);
                    }
                    else
                    {
                        MessageBox.Show("Поле \"Название группы\" должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Поле \"Вид\" должно быть выбрано!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            int index = e.RowIndex;

            selectedRowType = (int)dataGridView1.Rows[index].Cells[0].Value;
        }
    }
}
