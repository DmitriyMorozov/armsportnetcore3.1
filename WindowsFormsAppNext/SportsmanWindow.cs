﻿using DataBaseApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class SportsmanWindow : Form
    {
        private bool isEdit = false;
        private Sportsman? selectedRow;
        private RestClient client;

        // Создайте словарь с переводами сообщений об ошибках
        private Dictionary<string, string> errorMessages = new Dictionary<string, string>
        {
            { "FIO", "Ошибка! Фамилия, имя и отчество должны писаться с заглавной буквы и разделяться пробелом." },
            { "INN", "Ошибка! В поле ИНН должно быть 11 символов!" },
            { "PhoneNumber", "Ошибка! Номер телефона должен начинаться с +7 и иметь не менее 12 символов!" },
        };

        public SportsmanWindow(RestClient client, int id = -1)
        {
            InitializeComponent();
            this.client = client;



            if (id > 0)
            {
                selectedRow = client.Get<Sportsman>("api/Sportsman", id);
                isEdit = true;

                FIOTextBox.Text = selectedRow.FIO;
                INNTextBox.Text = selectedRow.INN;
                PhoneTextBox.Text = selectedRow.PhoneNumber;
                BirthDatePicker.Value = selectedRow.BirthDate;
            }
        }

        public bool checkUnique(string INN)
        {
            return !(client.Get<Sportsman>("api/Sportsman").FirstOrDefault(x => x.INN == INN) != null);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Sportsman sportsman = new Sportsman();
            string FIO = FIOTextBox.Text;
            string INN = INNTextBox.Text;
            string Phone = PhoneTextBox.Text;

            if (string.IsNullOrWhiteSpace(FIO) || string.IsNullOrWhiteSpace(INN) || string.IsNullOrWhiteSpace(Phone))
            {
                MessageBox.Show("Поля ФИО/ИНН/Телефон должны быть введены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DateTime? selectedDate = BirthDatePicker.Value;

            if (!selectedDate.HasValue)
            {
                MessageBox.Show("Не была выбрана дата рождения!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!isEdit)
            {
                sportsman = new Sportsman { FIO = FIO, INN = INN, PhoneNumber = Phone, BirthDate = (DateTime)selectedDate };
                //client.Post("api/Sportsman", sportsman);

                if (checkUnique(INN))
                {
                    client.Post("api/Sportsman", sportsman);
                }
                else
                {
                    MessageBox.Show("Поле ИНН должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                selectedRow.FIO = FIO;
                selectedRow.INN = INN;
                selectedRow.PhoneNumber = Phone;
                selectedRow.BirthDate = (DateTime)selectedDate;

                if (checkUnique(INN))
                {
                    client.Put("api/Sportsman", selectedRow);
                }
                else
                {
                    MessageBox.Show("Поле ИНН должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (client.IsOk())
            {
                string successMessage = isEdit ? "Запись успешно изменена!" : "Запись успешно добавлена!";
                MessageBox.Show(successMessage, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                HandleErrors();
            }
        }

        private void HandleErrors()
        {
            dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());

            if (errorContent != null && errorContent.errors != null)
            {
                foreach (var fieldErrors in errorContent.errors)
                {
                    string fieldName = fieldErrors.Name;
                    if (errorMessages.ContainsKey(fieldName))
                    {
                        string errorMessage = errorMessages[fieldName];
                        string errorDetails = string.Join(Environment.NewLine, fieldErrors.Value);
                        MessageBox.Show($"{errorMessage}{Environment.NewLine}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Неизвестная ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Неизвестная ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
