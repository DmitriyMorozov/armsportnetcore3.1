﻿using DataBaseApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace WindowsFormsAppNext
{
    public partial class TimeTableWindow : Form
    {
        private bool isEdit = false;
        private TimeTable? selectedRow;
        private RestClient client;
        private int selectedRoom = -1;
        private int selectedCoach = -1;
        private int selectedGroup = -1;

        public TimeTableWindow(RestClient client, int id = -1)
        {
            InitializeComponent();
            this.client = client;

            TimeStartPicker.Format = DateTimePickerFormat.Time;
            TimeStartPicker.CustomFormat = "HH:MM";
            TimeStartPicker.ShowUpDown = true;

            TimeEndPicker.Format = DateTimePickerFormat.Time;
            TimeEndPicker.CustomFormat = "HH:MM";
            TimeEndPicker.ShowUpDown = true;

            var loadGroup = client.Get<Group>("api/Group");
            var loadCoach = client.Get<Coach>("api/Coach");
            var loadRoomGym = client.Get<RoomGym>("api/RoomGym");

            foreach (var row in loadGroup)
            {
                GroupDataGridView.Rows.Add(new object[]
                {
                    row.Id,
                    row.Name
                });
            }

            foreach (var row in loadCoach)
            {
                CoachDataGridView.Rows.Add(new object[]
                {
                    row.Id,
                    row.FIO
                });
            }

            foreach (var row in loadRoomGym)
            {
                RoomGymDataGridView.Rows.Add(new object[]
                {
                    row.Id,
                    row.Name
                });
            }

            if (loadGroup.Count > 0)
            {
                selectedGroup = (int)GroupDataGridView.Rows[0].Cells[0].Value;
            }

            if (loadCoach.Count > 0)
            {
                selectedCoach = (int)CoachDataGridView.Rows[0].Cells[0].Value;
            }

            if (loadRoomGym.Count > 0)
            {
                selectedRoom = (int)RoomGymDataGridView.Rows[0].Cells[0].Value;
            }

            GroupDataGridView.Columns["IdGroup"].Visible = false;
            CoachDataGridView.Columns["IdCoach"].Visible = false;
            RoomGymDataGridView.Columns["IdRoom"].Visible = false;

            if (id > 0)
            {
                selectedRow = client.Get<TimeTable>("api/TimeTable", id);
                isEdit = true;

                int count = 0;
                foreach (DataGridViewRow row in GroupDataGridView.Rows)
                {
                    if ((int)row.Cells["IdGroup"].Value == selectedRow.GroupId)
                    {
                        GroupDataGridView.CurrentCell = GroupDataGridView.Rows[count].Cells[1];
                        selectedGroup = (int)GroupDataGridView.Rows[count].Cells[0].Value;
                        break;
                    }

                    count++;
                }

                count = 0;
                foreach (DataGridViewRow row in CoachDataGridView.Rows)
                {
                    if ((int)row.Cells["IdCoach"].Value == selectedRow.CoachId)
                    {
                        CoachDataGridView.CurrentCell = CoachDataGridView.Rows[count].Cells[1];
                        selectedCoach = (int)CoachDataGridView.Rows[count].Cells[0].Value;
                        break;
                    }

                    count++;
                }

                count = 0;
                foreach (DataGridViewRow row in RoomGymDataGridView.Rows)
                {
                    if ((int)row.Cells["IdRoom"].Value == selectedRow.RoomGymId)
                    {
                        RoomGymDataGridView.CurrentCell = RoomGymDataGridView.Rows[count].Cells[1];
                        selectedRoom = (int)RoomGymDataGridView.Rows[count].Cells[0].Value;
                        break;
                    }

                    count++;
                }

                DatePicker.Value = selectedRow.Date;
                TimeStartPicker.Value = selectedRow.TimeStart;
                TimeEndPicker.Value = selectedRow.TimeEnd;
            }
        }

        public bool checkUnique(TimeTable current)
        {
            return !(client.Get<TimeTable>("api/TimeTable").FirstOrDefault(x =>
                x.Date.ToShortDateString() == current.Date.ToShortDateString() &&
                x.TimeStart.ToString() == current.TimeStart.ToString() &&
                x.TimeEnd.ToString() == current.TimeEnd.ToString() &&
                x.CoachId == current.CoachId &&
                x.RoomGymId == current.RoomGymId &&
                x.GroupId == current.GroupId
            ) != null);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            TimeTable timeTable = new TimeTable();

            if (selectedCoach == -1 || selectedRoom == -1 || selectedGroup == -1)
            {
                MessageBox.Show("Необходимо выбрать Тренера/Помещение/Группа!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DateTime? startTime = TimeStartPicker.Value.AddSeconds(-TimeStartPicker.Value.Second);
            DateTime? endTime = TimeEndPicker.Value.AddSeconds(-TimeEndPicker.Value.Second);
            DateTime? selectedDate = DatePicker.Value.AddSeconds(-DatePicker.Value.Second)
                                                     .AddMinutes(-DatePicker.Value.Minute)
                                                     .AddHours(-DatePicker.Value.Hour);


            if (startTime.HasValue && endTime.HasValue && selectedDate.HasValue)
            {

                if (!isEdit)
                {
                    timeTable = new TimeTable
                    {
                        Date = (DateTime)selectedDate,
                        RoomGymId = selectedRoom,
                        TimeStart = (DateTime)startTime,
                        TimeEnd = (DateTime)endTime,
                        CoachId = selectedCoach,
                        GroupId = selectedGroup
                    };
                }
                else
                {
                    timeTable = new TimeTable
                    {
                        Id = selectedRow.Id,
                        Date = (DateTime)selectedDate,
                        RoomGymId = selectedRoom,
                        TimeStart = (DateTime)startTime,
                        TimeEnd = (DateTime)endTime,
                        CoachId = selectedCoach,
                        GroupId = selectedGroup
                    };
                }
            }
            else
            {
                MessageBox.Show("Не было выбрано время начала/конца/дата!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!isEdit)
            {
                bool timeValidation = crossTimeValidation(timeTable);
                bool sameTimeValidation = timeTable.TimeStart == timeTable.TimeEnd;
                bool firstTimeNotLessSecond = timeTable.TimeStart < timeTable.TimeEnd;

                if (timeValidation && !sameTimeValidation && firstTimeNotLessSecond)
                {
                    if (checkUnique(timeTable))
                    {
                        client.Post("api/TimeTable", timeTable);
                    }
                    else
                    {
                        MessageBox.Show("Поле \"Название группы\" должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    //client.Post("api/TimeTable", timeTable);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Не верно введено время начала/конца!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
            }
            else
            {
                bool timeValidation = crossTimeValidation(timeTable);
                bool sameTimeValidation = timeTable.TimeStart == timeTable.TimeEnd;
                bool firstTimeNotLessSecond = timeTable.TimeStart < timeTable.TimeEnd;

                if (timeValidation && !sameTimeValidation && firstTimeNotLessSecond)
                {
                    if (checkUnique(timeTable))
                    {
                        client.Put("api/TimeTable", timeTable);
                    }
                    else
                    {
                        MessageBox.Show("Поле \"Название группы\" должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    //client.Put("api/TimeTable", timeTable);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Не верно введено время начала/конца!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
            }
        }



        private bool crossTimeValidation(TimeTable tt)
        {
            List<TimeTable> savedData = client.Get<TimeTable>("api/TimeTable").FindAll(el => el.Id != tt.Id);

            foreach (TimeTable time in savedData)
            {
                if ((tt.TimeStart >= time.TimeStart && tt.TimeStart.AddMinutes(1) <= time.TimeEnd) &&
                    tt.RoomGymId == time.RoomGymId &&
                    tt.Date == time.Date &&
                    tt.RoomGymId == time.RoomGymId)
                {
                    return false;
                }
            }

            return true;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CoachDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            int index = e.RowIndex;

            selectedCoach = (int)CoachDataGridView.Rows[index].Cells[0].Value;
        }

        private void GroupDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            int index = e.RowIndex;

            selectedGroup = (int)GroupDataGridView.Rows[index].Cells[0].Value;
        }

        private void RoomGymDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            int index = e.RowIndex;

            selectedRoom = (int)RoomGymDataGridView.Rows[index].Cells[0].Value;
        }
    }
}
