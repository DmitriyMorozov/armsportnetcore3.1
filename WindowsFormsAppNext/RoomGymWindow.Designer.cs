﻿using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class RoomGymWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            saveButton = new Button();
            cancelButton = new Button();
            label1 = new Label();
            RoomTextBox = new TextBox();
            SuspendLayout();
            // 
            // saveButton
            // 
            saveButton.Location = new System.Drawing.Point(12, 217);
            saveButton.Name = "saveButton";
            saveButton.Size = new System.Drawing.Size(75, 23);
            saveButton.TabIndex = 0;
            saveButton.Text = "Сохранить";
            saveButton.UseVisualStyleBackColor = true;
            saveButton.Click += saveButton_Click;
            // 
            // cancelButton
            // 
            cancelButton.Location = new System.Drawing.Point(181, 217);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.TabIndex = 1;
            cancelButton.Text = "Отмена";
            cancelButton.UseVisualStyleBackColor = true;
            cancelButton.Click += cancelButton_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(89, 81);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(75, 15);
            label1.TabIndex = 2;
            label1.Text = "Помещение";
            // 
            // RoomTextBox
            // 
            RoomTextBox.Location = new System.Drawing.Point(63, 99);
            RoomTextBox.Name = "RoomTextBox";
            RoomTextBox.Size = new System.Drawing.Size(131, 23);
            RoomTextBox.TabIndex = 3;
            // 
            // RoomGymWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(266, 252);
            Controls.Add(RoomTextBox);
            Controls.Add(label1);
            Controls.Add(cancelButton);
            Controls.Add(saveButton);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Name = "RoomGymWindow";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Помещение";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button saveButton;
        private Button cancelButton;
        private Label label1;
        private TextBox RoomTextBox;
    }
}