﻿using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            dataGridView = new DataGridView();
            contextMenuStrip1 = new ContextMenuStrip(components);
            menuStrip1 = new MenuStrip();
            справочникиToolStripMenuItem = new ToolStripMenuItem();
            sportsmanToolStripMenuItem = new ToolStripMenuItem();
            coachToolStripMenuItem = new ToolStripMenuItem();
            classTypeToolStripMenuItem = new ToolStripMenuItem();
            roomGymToolStripMenuItem = new ToolStripMenuItem();
            GroupToolStripMenuItem = new ToolStripMenuItem();
            GroupMembersToolStripMenuItem = new ToolStripMenuItem();
            TimeTableToolStripMenuItem = new ToolStripMenuItem();
            расписанияToolStripMenuItem = new ToolStripMenuItem();
            MainSheduleToolStripMenuItem = new ToolStripMenuItem();
            CoachSheduleToolStripMenuItem = new ToolStripMenuItem();
            RoomGymSheduleToolStripMenuItem = new ToolStripMenuItem();
            addButton = new Button();
            deleteButton = new Button();
            editButton = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridView).BeginInit();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // dataGridView
            // 
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeColumns = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView.Location = new System.Drawing.Point(6, 72);
            dataGridView.Margin = new Padding(2, 1, 2, 1);
            dataGridView.MultiSelect = false;
            dataGridView.Name = "dataGridView";
            dataGridView.ReadOnly = true;
            dataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridView.RowTemplate.Height = 41;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.Size = new System.Drawing.Size(889, 412);
            dataGridView.TabIndex = 0;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            menuStrip1.Items.AddRange(new ToolStripItem[] { справочникиToolStripMenuItem, расписанияToolStripMenuItem });
            menuStrip1.Location = new System.Drawing.Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Padding = new Padding(3, 1, 0, 1);
            menuStrip1.Size = new System.Drawing.Size(902, 24);
            menuStrip1.TabIndex = 2;
            menuStrip1.Text = "menuStrip1";
            // 
            // справочникиToolStripMenuItem
            // 
            справочникиToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { sportsmanToolStripMenuItem, coachToolStripMenuItem, classTypeToolStripMenuItem, roomGymToolStripMenuItem, GroupToolStripMenuItem, GroupMembersToolStripMenuItem, TimeTableToolStripMenuItem });
            справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            справочникиToolStripMenuItem.Size = new System.Drawing.Size(94, 22);
            справочникиToolStripMenuItem.Text = "Справочники";
            // 
            // sportsmanToolStripMenuItem
            // 
            sportsmanToolStripMenuItem.Name = "sportsmanToolStripMenuItem";
            sportsmanToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            sportsmanToolStripMenuItem.Text = "Спортсмены";
            sportsmanToolStripMenuItem.Click += sportsmanToolStripMenuItem_Click;
            // 
            // coachToolStripMenuItem
            // 
            coachToolStripMenuItem.Name = "coachToolStripMenuItem";
            coachToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            coachToolStripMenuItem.Text = "Тренеры";
            coachToolStripMenuItem.Click += coachToolStripMenuItem_Click;
            // 
            // classTypeToolStripMenuItem
            // 
            classTypeToolStripMenuItem.Name = "classTypeToolStripMenuItem";
            classTypeToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            classTypeToolStripMenuItem.Text = "Вид спорта";
            classTypeToolStripMenuItem.Click += classTypeToolStripMenuItem_Click;
            // 
            // roomGymToolStripMenuItem
            // 
            roomGymToolStripMenuItem.Name = "roomGymToolStripMenuItem";
            roomGymToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            roomGymToolStripMenuItem.Text = "Помещение";
            roomGymToolStripMenuItem.Click += roomGymToolStripMenuItem_Click;
            // 
            // GroupToolStripMenuItem
            // 
            GroupToolStripMenuItem.Name = "GroupToolStripMenuItem";
            GroupToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            GroupToolStripMenuItem.Text = "Группа";
            GroupToolStripMenuItem.Click += GroupToolStripMenuItem_Click;
            // 
            // GroupMembersToolStripMenuItem
            // 
            GroupMembersToolStripMenuItem.Name = "GroupMembersToolStripMenuItem";
            GroupMembersToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            GroupMembersToolStripMenuItem.Text = "Участники групп";
            GroupMembersToolStripMenuItem.Click += GroupMembersToolStripMenuItem_Click;
            // 
            // TimeTableToolStripMenuItem
            // 
            TimeTableToolStripMenuItem.Name = "TimeTableToolStripMenuItem";
            TimeTableToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            TimeTableToolStripMenuItem.Text = "Время занятий";
            TimeTableToolStripMenuItem.Click += TimeTableToolStripMenuItem_Click;
            // 
            // расписанияToolStripMenuItem
            // 
            расписанияToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { MainSheduleToolStripMenuItem, CoachSheduleToolStripMenuItem, RoomGymSheduleToolStripMenuItem });
            расписанияToolStripMenuItem.Name = "расписанияToolStripMenuItem";
            расписанияToolStripMenuItem.Size = new System.Drawing.Size(84, 22);
            расписанияToolStripMenuItem.Text = "Расписания";
            // 
            // MainSheduleToolStripMenuItem
            // 
            MainSheduleToolStripMenuItem.Name = "MainSheduleToolStripMenuItem";
            MainSheduleToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            MainSheduleToolStripMenuItem.Text = "Общее расписание";
            MainSheduleToolStripMenuItem.Click += MainSheduleToolStripMenuItem_Click;
            // 
            // CoachSheduleToolStripMenuItem
            // 
            CoachSheduleToolStripMenuItem.Name = "CoachSheduleToolStripMenuItem";
            CoachSheduleToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            CoachSheduleToolStripMenuItem.Text = "Расписание тренеров";
            CoachSheduleToolStripMenuItem.Click += CoachSheduleToolStripMenuItem_Click;
            // 
            // RoomGymSheduleToolStripMenuItem
            // 
            RoomGymSheduleToolStripMenuItem.Name = "RoomGymSheduleToolStripMenuItem";
            RoomGymSheduleToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            RoomGymSheduleToolStripMenuItem.Text = "Расписание помещений";
            RoomGymSheduleToolStripMenuItem.Click += RoomGymSheduleToolStripMenuItem_Click;
            // 
            // addButton
            // 
            addButton.Location = new System.Drawing.Point(6, 25);
            addButton.Margin = new Padding(2, 1, 2, 1);
            addButton.Name = "addButton";
            addButton.Size = new System.Drawing.Size(117, 22);
            addButton.TabIndex = 3;
            addButton.Text = "Добавить запись";
            addButton.UseVisualStyleBackColor = true;
            addButton.Click += addButton_Click;
            // 
            // deleteButton
            // 
            deleteButton.Location = new System.Drawing.Point(126, 25);
            deleteButton.Margin = new Padding(2, 1, 2, 1);
            deleteButton.Name = "deleteButton";
            deleteButton.Size = new System.Drawing.Size(104, 22);
            deleteButton.TabIndex = 4;
            deleteButton.Text = "Удалить запись";
            deleteButton.UseVisualStyleBackColor = true;
            deleteButton.Click += deleteButton_Click;
            // 
            // editButton
            // 
            editButton.Location = new System.Drawing.Point(234, 25);
            editButton.Margin = new Padding(2, 1, 2, 1);
            editButton.Name = "editButton";
            editButton.Size = new System.Drawing.Size(117, 22);
            editButton.TabIndex = 5;
            editButton.Text = "Изменить запись";
            editButton.UseVisualStyleBackColor = true;
            editButton.Click += editButton_Click;
            // 
            // MainWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(902, 494);
            Controls.Add(editButton);
            Controls.Add(deleteButton);
            Controls.Add(addButton);
            Controls.Add(menuStrip1);
            Controls.Add(dataGridView);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MainMenuStrip = menuStrip1;
            Margin = new Padding(2, 1, 2, 1);
            Name = "MainWindow";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Главное Окно";
            ((System.ComponentModel.ISupportInitialize)dataGridView).EndInit();
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DataGridView dataGridView;
        private ContextMenuStrip contextMenuStrip1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem справочникиToolStripMenuItem;
        private ToolStripMenuItem sportsmanToolStripMenuItem;
        private ToolStripMenuItem coachToolStripMenuItem;
        private Button addButton;
        private Button deleteButton;
        private Button editButton;
        private ToolStripMenuItem classTypeToolStripMenuItem;
        private ToolStripMenuItem roomGymToolStripMenuItem;
        private ToolStripMenuItem GroupToolStripMenuItem;
        private ToolStripMenuItem GroupMembersToolStripMenuItem;
        private ToolStripMenuItem TimeTableToolStripMenuItem;
        private ToolStripMenuItem расписанияToolStripMenuItem;
        private ToolStripMenuItem MainSheduleToolStripMenuItem;
        private ToolStripMenuItem CoachSheduleToolStripMenuItem;
        private ToolStripMenuItem RoomGymSheduleToolStripMenuItem;
    }
}