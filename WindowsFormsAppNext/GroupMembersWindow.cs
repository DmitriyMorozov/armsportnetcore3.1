﻿using DataBaseApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class GroupMembersWindow : Form
    {
        private bool isEdit = false;
        private GroupMembers? selectedRow;
        private RestClient client;
        private int selectedRowSportsman = -1;
        private int selectedRowGroup = -1;

        public GroupMembersWindow(RestClient client, int id = -1)
        {
            InitializeComponent();
            this.client = client;

            var loadSportsman = client.Get<Sportsman>("api/Sportsman");
            var loadGroup = client.Get<Group>("api/Group");

            foreach (var row in loadSportsman)
            {
                SportDataGridView.Rows.Add(new object[]
                {
                    row.Id,
                    row.FIO
                });
            }

            foreach (var row in loadGroup)
            {
                GroupDataGridView.Rows.Add(new object[]
                {
                    row.Id,
                    row.Name
                });
            }

            if (loadSportsman.Count > 0)
            {
                selectedRowSportsman = (int)SportDataGridView.Rows[0].Cells[0].Value;
            }

            if (loadGroup.Count > 0)
            {
                selectedRowGroup = (int)GroupDataGridView.Rows[0].Cells[0].Value;
            }

            SportDataGridView.Columns["Id"].Visible = false;
            GroupDataGridView.Columns["IdGroup"].Visible = false;

            if (id > 0)
            {
                selectedRow = client.Get<GroupMembers>("api/GroupMembers", id);
                isEdit = true;

                int count = 0;
                foreach (DataGridViewRow row in SportDataGridView.Rows)
                {
                    if ((int)row.Cells["Id"].Value == selectedRow.SportsmanId)
                    {
                        SportDataGridView.CurrentCell = SportDataGridView.Rows[count].Cells[1];
                        selectedRowSportsman = (int)SportDataGridView.Rows[count].Cells[0].Value;
                        break;
                    }

                    count++;
                }

                count = 0;
                foreach (DataGridViewRow row in GroupDataGridView.Rows)
                {
                    if ((int)row.Cells["IdGroup"].Value == selectedRow.GroupId)
                    {
                        GroupDataGridView.CurrentCell = GroupDataGridView.Rows[count].Cells[1];
                        selectedRowGroup = (int)GroupDataGridView.Rows[count].Cells[0].Value;
                        break;
                    }

                    count++;
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            GroupMembers groupMembers = new GroupMembers();

            if (selectedRowSportsman != -1 && selectedRowGroup != -1)
            {
                if (!isEdit)
                {
                    // Проверяем, не существует ли уже записи с выбранными SportsmanId и GroupId
                    var existingGroupMember = client.Get<GroupMembers>("api/GroupMembers")
                        .FirstOrDefault(x => x.SportsmanId == selectedRowSportsman && x.GroupId == selectedRowGroup);

                    if (existingGroupMember != null)
                    {
                        MessageBox.Show("Запись с выбранным Спортсменом и Группой уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    groupMembers = new GroupMembers { SportsmanId = selectedRowSportsman, GroupId = selectedRowGroup };

                    client.Post("api/GroupMembers", groupMembers);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    groupMembers.Id = selectedRow.Id;
                    groupMembers.SportsmanId = selectedRowSportsman;
                    groupMembers.GroupId = selectedRowGroup;

                    client.Put("api/GroupMembers", groupMembers);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Должен быть выбран Спортсмен и Группа!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SportDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            int index = e.RowIndex;

            selectedRowSportsman = (int)SportDataGridView.Rows[index].Cells[0].Value;
        }

        private void GroupDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            int index = e.RowIndex;

            selectedRowGroup = (int)GroupDataGridView.Rows[index].Cells[0].Value;
        }
    }
}
