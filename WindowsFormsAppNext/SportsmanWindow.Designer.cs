﻿using System;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    partial class SportsmanWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            SaveButton = new Button();
            label1 = new Label();
            FIOTextBox = new TextBox();
            INNTextBox = new TextBox();
            label2 = new Label();
            PhoneTextBox = new TextBox();
            label3 = new Label();
            label4 = new Label();
            BirthDatePicker = new DateTimePicker();
            CancelButton = new Button();
            SuspendLayout();
            // 
            // SaveButton
            // 
            SaveButton.Location = new System.Drawing.Point(6, 332);
            SaveButton.Margin = new Padding(2, 1, 2, 1);
            SaveButton.Name = "SaveButton";
            SaveButton.Size = new System.Drawing.Size(81, 22);
            SaveButton.TabIndex = 0;
            SaveButton.Text = "Сохранить";
            SaveButton.UseVisualStyleBackColor = true;
            SaveButton.Click += SaveButton_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(13, 6);
            label1.Margin = new Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(34, 15);
            label1.TabIndex = 4;
            label1.Text = "ФИО";
            // 
            // FIOTextBox
            // 
            FIOTextBox.Location = new System.Drawing.Point(13, 29);
            FIOTextBox.Margin = new Padding(2, 1, 2, 1);
            FIOTextBox.MaxLength = 50;
            FIOTextBox.Name = "FIOTextBox";
            FIOTextBox.Size = new System.Drawing.Size(199, 23);
            FIOTextBox.TabIndex = 5;
            // 
            // INNTextBox
            // 
            INNTextBox.Location = new System.Drawing.Point(13, 76);
            INNTextBox.Margin = new Padding(2, 1, 2, 1);
            INNTextBox.MaxLength = 11;
            INNTextBox.Name = "INNTextBox";
            INNTextBox.Size = new System.Drawing.Size(199, 23);
            INNTextBox.TabIndex = 7;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(13, 53);
            label2.Margin = new Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(34, 15);
            label2.TabIndex = 6;
            label2.Text = "ИНН";
            // 
            // PhoneTextBox
            // 
            PhoneTextBox.Location = new System.Drawing.Point(13, 124);
            PhoneTextBox.Margin = new Padding(2, 1, 2, 1);
            PhoneTextBox.MaxLength = 12;
            PhoneTextBox.Name = "PhoneTextBox";
            PhoneTextBox.Size = new System.Drawing.Size(199, 23);
            PhoneTextBox.TabIndex = 9;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(13, 101);
            label3.Margin = new Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(74, 15);
            label3.TabIndex = 8;
            label3.Text = "Моб. номер";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(13, 153);
            label4.Margin = new Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(90, 15);
            label4.TabIndex = 10;
            label4.Text = "Дата рождения";
            // 
            // BirthDatePicker
            // 
            BirthDatePicker.Location = new System.Drawing.Point(13, 176);
            BirthDatePicker.Margin = new Padding(2, 1, 2, 1);
            BirthDatePicker.Name = "BirthDatePicker";
            BirthDatePicker.Size = new System.Drawing.Size(199, 23);
            BirthDatePicker.TabIndex = 11;
            BirthDatePicker.Value = new DateTime(2019, 10, 3, 23, 45, 52, 299);
            // 
            // CancelButton
            // 
            CancelButton.Location = new System.Drawing.Point(183, 332);
            CancelButton.Margin = new Padding(2, 1, 2, 1);
            CancelButton.Name = "CancelButton";
            CancelButton.Size = new System.Drawing.Size(103, 22);
            CancelButton.TabIndex = 12;
            CancelButton.Text = "Отмена";
            CancelButton.UseVisualStyleBackColor = true;
            CancelButton.Click += CancelButton_Click;
            // 
            // SportsmanWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(292, 359);
            Controls.Add(CancelButton);
            Controls.Add(BirthDatePicker);
            Controls.Add(label4);
            Controls.Add(PhoneTextBox);
            Controls.Add(label3);
            Controls.Add(INNTextBox);
            Controls.Add(label2);
            Controls.Add(FIOTextBox);
            Controls.Add(label1);
            Controls.Add(SaveButton);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Margin = new Padding(2, 1, 2, 1);
            Name = "SportsmanWindow";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Спортсмены";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button SaveButton;
        private Label label1;
        private TextBox FIOTextBox;
        private TextBox INNTextBox;
        private Label label2;
        private TextBox PhoneTextBox;
        private Label label3;
        private Label label4;
        private DateTimePicker BirthDatePicker;
        private Button CancelButton;
    }
}