﻿using DataBaseApi;
using InterfaceLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppNext
{
    public partial class ClassTypeWindow : Form
    {
        private RestClient client;
        private bool isEdit = false;
        private ClassType? selectedRow;

        public ClassTypeWindow(RestClient client, int id = -1)
        {
            InitializeComponent();
            this.client = client;

            if (id > 0)
            {
                selectedRow = client.Get<ClassType>("api/ClassType", id);
                isEdit = true;

                typeTextBox.Text = selectedRow.Name;
            }
        }

        public bool checkUnique(string name)
        {

            if (isEdit)
            {
                return !(client.Get<ClassType>("api/ClassType").FirstOrDefault(x => x.Name == name && x.Id != selectedRow.Id) != null);
            }
            else
            {
                return !(client.Get<ClassType>("api/ClassType").FirstOrDefault(x => x.Name == name) != null);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            ClassType type = new ClassType();
            string strType = typeTextBox.Text;

            if (strType == "")
            {
                MessageBox.Show("Поле наименование должно быть заполнено!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!isEdit)
            {
                type = new ClassType { Name = strType };

                if (checkUnique(strType))
                {
                    client.Post("api/ClassType", type);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно добавлена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Поле наименование должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {

                selectedRow.Name = strType;

                if (checkUnique(strType))
                {
                    client.Put("api/ClassType", selectedRow);

                    if (client.IsOk())
                    {
                        MessageBox.Show("Запись успешно изменена!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        dynamic errorContent = JsonConvert.DeserializeObject(client.GetContent());
                        MessageBox.Show(errorContent["errors"].ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    isEdit = false;
                }
                else
                {
                    MessageBox.Show("Поле наименование должно быть уникальным!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
